FROM openjdk:17-jdk

COPY app.jar /app/target/SpringBootEx-0.0.1-SNAPSHOT.jar

WORKDIR /app

ENTRYPOINT ["java", "-jar", "app.jar"]