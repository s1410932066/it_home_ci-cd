package com.example.springbootex.architecture;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/user")
public class UserController {
    private final UserService service;

    @GetMapping("/get")
    public String get(@RequestParam("account") String account){
        return service.checkUser(account);
    }

    @PostMapping("/post/1")
    public User register(
            @RequestParam("account") String account,
            @RequestParam("password")String password
            ){
        return service.register(account, password);
    }

    @Operation(description = "註冊用戶")
    @PostMapping("/post/2")
    public User register(
            @RequestBody User user
    ){
        return service.register2(user);
    }

    @GetMapping("/getAll")
    public List<User> allUser(){
        return service.allUser();
    }
}
