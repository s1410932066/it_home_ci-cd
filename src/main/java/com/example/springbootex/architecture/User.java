package com.example.springbootex.architecture;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "_user")
@Schema(name = "User", description = "用戶資料表")
public class User {
    @Id
    @GeneratedValue
    private Long id;
    @Schema(description = "帳號")
    private String account;
    @Schema(description = "密碼")
    private String password;
}
