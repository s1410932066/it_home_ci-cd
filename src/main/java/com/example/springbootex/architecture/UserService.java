package com.example.springbootex.architecture;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class UserService {
    private final UserRepository userRepository;
    public String checkUser(String account) {
        var user = userRepository.findByAccount(account);
        if (user.isPresent()){
            return account + "已有用戶";
        }else {
            return account + "查無此用戶";
        }

    }

    public User register(String account, String password) {
        var user = User.builder()
                .account(account)
                .password(password)
                .build();
        userRepository.save(user);
        return user;
    }

    public User register2(User user) {
        userRepository.save(user);
        return user;
    }

    public List<User> allUser() {
        var UserList = userRepository.findAllUser();
        return UserList;
    }
}
