package com.example.springbootex.FCM;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping("/fcm")
public class FcmController {
    private final FcmService service;

    @PostMapping("/send")
    public void sendNotification(@RequestBody NotificationRequest request) {
        // 發送通知
        service.sendNotification(request);
    }
}
