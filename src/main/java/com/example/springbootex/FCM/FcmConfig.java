package com.example.springbootex.FCM;

import com.google.auth.oauth2.GoogleCredentials;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.FileSystemResource;

import java.io.FileInputStream;

@Configuration
@Slf4j
public class FcmConfig {
    @Bean
    public FirebaseApp initializeFirebaseApp() {
        try {
            // 載入Firebase Admin SDK的設定
            FirebaseOptions options = FirebaseOptions.builder()
                    .setCredentials(GoogleCredentials.fromStream(new FileInputStream("firebase-adminsdk.json")))
                    .build();

            // 初始化Firebase App
            FirebaseApp firebaseApp = FirebaseApp.initializeApp(options);
            // 檢查FirebaseApp是否初始化成功
            log.info("FirebaseApp 初始化 成功!");
            // 初始化Firebase App
            return firebaseApp;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}
