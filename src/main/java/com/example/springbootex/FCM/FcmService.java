package com.example.springbootex.FCM;

import com.google.firebase.messaging.FirebaseMessaging;
import com.google.firebase.messaging.FirebaseMessagingException;
import com.google.firebase.messaging.Message;
import com.google.firebase.messaging.Notification;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
public class FcmService {
    public void sendNotification(NotificationRequest request) {
        FirebaseMessaging messaging = FirebaseMessaging.getInstance();
        Message message = Message.builder()
                .setNotification(Notification
                        .builder()
                        .setTitle(request.getTitle())
                        .setBody(request.getBody())
                        .build())
                .putData("title", request.getTitle())
                .putData("body", request.getBody())
                .setToken(request.getFcm_token())
                .build();

        try {
            messaging.send(message);
        } catch (FirebaseMessagingException e) {
            e.printStackTrace();
        }
    }
}
