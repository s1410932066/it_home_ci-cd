package com.example.springbootex;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Tag(
        name = "你馬死了"
)
@Controller
//@RequestMapping("/api/t")
public class HelloWorld {

    @Operation(summary = "你爸死了")
    @GetMapping("/hello")
    public ResponseEntity<String> hello1(){
        return ResponseEntity.ok("hello 你好!!");
    }

    @GetMapping("/hello2")
    @ResponseBody
    public String hello2(){
        return "Hello World !!!";
    }

    @GetMapping("/hello3")
    public String hello3(){
        return "hello3";
    }
}
