package com.example.springbootex;

import com.example.springbootex.architecture.User;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/p")
public class Parameter {
    @GetMapping("/url/{id}")
    public String url(@PathVariable("id") String id){
        return id;
    }

    @GetMapping("/query")
    public String query(@RequestParam("id")String id){
        return id;
    }

    @GetMapping("/jsonBody")
    public User jsonBody (@RequestBody User user){
        return user;
    }

    @GetMapping("/formData")
    public String formData (@RequestPart("id") String id){
        return id;
    }

    @GetMapping("/user-agent")
    public String getUserAgent(@RequestHeader("User-Agent") String userAgent) {
        return "User-Agent header value: " + userAgent;
    }

    @GetMapping("/cookie")
    public String getCookieValue(@CookieValue("sessionId") String sessionId) {
        return "Session ID from Cookie: " + sessionId;
    }
}
