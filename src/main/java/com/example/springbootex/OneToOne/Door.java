package com.example.springbootex.OneToOne;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@Entity
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Door {
    @Id
    @GeneratedValue
    private Long id;
    private String doorName;
    private String type;

    /**
     * OneToMany 與 ManyToOne
     */
    @OneToMany(cascade = CascadeType.ALL,
            fetch = FetchType.EAGER,
            orphanRemoval = true,
            mappedBy = "door")
    @JsonManagedReference
    private List<Key> keys;

    /**
     * ManyToMany
     */
//    @ManyToMany(mappedBy = "door")
////    @JsonIgnore
//    @JsonIgnoreProperties("door")
//    @Builder.Default
//    private List<Key> key = new ArrayList<>();
}
