package com.example.springbootex.OneToOne;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@Entity
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Key {
    @Id
    @GeneratedValue
    private Long id;
    private String keyName;


    /**
     * OneToOne
     */
//    @OneToOne(cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.EAGER)
//    @JoinColumn(name = "door_id")
////    @JsonBackReference
//    private Door door;

    /**
     * ManyToOne 與 OneToMany
     */
    @ManyToOne
    @JoinColumn(name = "door_id")
    @JsonBackReference
    private Door door;

    /**
     * ManyToMany
     */
//    @ManyToMany
////    @JsonIgnore
//    @JoinTable(
//            name = "door_key",
//            joinColumns = @JoinColumn(name = "key_id"),
//            inverseJoinColumns = @JoinColumn(name = "door_id")
//    )
//    @JsonIgnoreProperties("key")
//    @Builder.Default
//    private List<Door> door = new ArrayList<>();
}
