package com.example.springbootex.OneToOne;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
public class AssociateController {
    private final DoorRepository doorRepository;
    private final KeyRepository keyRepository;
    @PostMapping("/ex")
    public String ex(){
        var door = Door.builder()
                .type("open")
                .doorName("digDoor")
                .build();
        doorRepository.save(door);
        var key1 = Key.builder()
                .keyName("Key1")
                .door(door) //這裡將此鑰匙著名與剛剛創建的門有關聯
                .build();
        keyRepository.save(key1);
        var key2 = Key.builder()
                .keyName("Key2")
                .door(door) //這裡將此鑰匙著名與剛剛創建的門有關聯
                .build();
        keyRepository.save(key2);
        return "Good";
    }

    @PostMapping("/door")
    public Door Door(@RequestBody Door door){
        doorRepository.save(door);
        return door;
    }
    @GetMapping("/doorCheck")
    public Door Door(){
        var door = doorRepository.findById(1L).orElseThrow();
        return door;
    }

    @PostMapping("/key")
    public Key Door(@RequestBody Key key){
        keyRepository.save(key);
        return key;
    }

//    @PostMapping("/many")
//    public Door createDoorWithKey(){
//        var door = Door
//                .builder()
//                .doorName("一號門")
//                .type("open")
//                .build();
//        doorRepository.save(door);
//
//        var key = Key
//                .builder()
//                .keyName("多功能鑰匙")
//                .build();
//        keyRepository.save(key);
//
//        door.getKey().add(key);
//        key.getDoor().add(door);
//
//        doorRepository.save(door);
//        return door;
//    }
}
