package com.example.springbootex.OneToOne;

import lombok.Data;

@Data
public class ManyRequest {
    private String doorName;
    private String type;
    private String keyName;
}
