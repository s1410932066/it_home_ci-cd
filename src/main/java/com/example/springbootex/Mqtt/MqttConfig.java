package com.example.springbootex.Mqtt;

import com.hivemq.client.internal.mqtt.MqttRxClient;
import com.hivemq.client.mqtt.mqtt5.Mqtt5AsyncClient;
import com.hivemq.client.mqtt.mqtt5.Mqtt5BlockingClient;
import com.hivemq.client.mqtt.mqtt5.Mqtt5Client;
import com.hivemq.client.mqtt.mqtt5.Mqtt5RxClient;
import com.hivemq.client.mqtt.mqtt5.message.publish.Mqtt5Publish;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.nio.charset.StandardCharsets;

@Slf4j
@Configuration
@RequiredArgsConstructor
public class MqttConfig {

    private final String topic = "test/"; // 訂閱的 MQTT 主題
    // 客戶端識別ID
    String clientId = "clientA";
    // MQTT 伺服器位址
    String broker = "broker.hivemq.com";

    @Bean
    public void connectMqttClient() {
        //創建MQTT5Client
        Mqtt5AsyncClient client = Mqtt5Client.builder()
                .identifier(clientId)
                .serverHost(broker)
                .serverPort(1883)
                .buildAsync();
        //連接
        client.connect()
                .whenComplete((connAck, throwable) -> {
                    if (throwable != null) {
                        //發生錯誤時
                        log.error(throwable.getMessage());
                    } else {
                        //成功連接時
                        log.error("連接成功");
                        //主動訂閱主題
                        client.subscribeWith()
                                .topicFilter(topic)
                                .callback(this::onMessageReceived)  //將發送到主題中的訊息做處理
                                .send()
                                .whenComplete((subAck, throwable_sub) -> {
                                    if (throwable_sub != null) {
                                        log.error(throwable_sub.getMessage());
                                    } else {
                                        log.error("訂閱 " + topic + "主題 成功");
                                    }
                                });
                    }
        });
    }

    private void onMessageReceived(Mqtt5Publish publish) {
        String topic = publish.getTopic().toString();
        String payload = new String(publish.getPayloadAsBytes(), StandardCharsets.UTF_8);
        log.error("Received message on topic '" + topic + "': " + payload);
    }


}
